# Stream Data (Input Output)

```
Keterangan:
Mengenal I/O data stream sebelum C++ Hello World
```

Bagi teman-teman yang sudah pernah menggunakan sistem operasi unix-like
mungkin sudah tidak asing lagi. Karena pada dasarnya sistem operasi
tersebut berbasis teks.
Tahukah ketika anda menggunakan shell atau terminal, saat itu anda akan sering berinterasksi dengan STDIN, STDOUT, dan STDERR. 3 Hal tersebut merupakan
standarisasi dari POSIX shell.

0 STDIN => menginputkan data  
1 STDOUT => mencetak teks ke output  
2 STDERR => memberi tahu status program

Sebuah program akan berjalan dengan sukses jika return bernilai 0, selain nilai itu maka akan ada kesalahan.

Baca selengkapnya:

- [https://en.wikipedia.org/wiki/Standard_streams](https://en.wikipedia.org/wiki/Standard_streams)
- [https://stackoverflow.com/questions/3385201/confused-about-stdin-stdout-and-stderr](https://stackoverflow.com/questions/3385201/confused-about-stdin-stdout-and-stderr)
- [https://www.howtogeek.com/435903/what-are-stdin-stdout-and-stderr-on-linux/](https://www.howtogeek.com/435903/what-are-stdin-stdout-and-stderr-on-linux/)
- [https://opensource.com/article/18/10/linux-data-streams](https://opensource.com/article/18/10/linux-data-streams)
